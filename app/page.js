"use client"
import { useUserAgent } from 'next-useragent'
import React from 'react'

export default function Page(){

    const [ua, setUA] = React.useState(getUA)

    return(
            <>
            {(ua.isMobile) ?  (
                    <div className = {"fixed bottom-0 w-11/12 h-80 left-1/2 -translate-x-1/2 bg-slate-300 opacity-50 rounded-xl shadow-xl shadow-slate-300"}></div>
            ) :
            (
                    <div className = {"fixed top-1/2 -translate-y-1/2 right-1/4 translate-x-1/2  w-96 h-96  bg-slate-300 opacity-50 rounded-xl shadow-xl shadow-slate-300"}></div>
            )}
            </>
    )

}

function getUA(){
    if (typeof window !== "undefined") {
        return useUserAgent(window.navigator.useragent)
    }
    return null
}